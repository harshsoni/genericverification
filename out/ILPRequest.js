"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ILPConstants {
}
ILPConstants.VerifyUserSolutionFirstLinePattern = "(\\s)*#(\\s)*(PF|OOP|OOPR|DSA|PBL[0-9]*|USM[0-9]*|SCM[2-3]|FSM[0-9])(\\s)*(-)(\\s)*(Assgn|Exer|Prac)(\\s)*(-)(\\s)*[1-9][0-9]{0,4}(\\s)*";
ILPConstants.VerifyUserSolutionTryoutFirstLinePattern = "(\\s)*#(\\s)*(PF|OOP|OOPR|DSA|PBL[0-9]*|USM[0-9]*|SCM[2-3]|FSM[0-9])(\\s)*(-)(\\s)*(Tryout)(\\s)*";
ILPConstants.VerifyRequestType = "VerifySolution";
ILPConstants.ReadUserInfoSuccess = "SUCCESS";
ILPConstants.ReadUserInfoFailure = "FAILURE";
ILPConstants.ILPRequestHandsOnType = "Assignment";
ILPConstants.ILPRequestModuleName = "Programming";
ILPConstants.ILPRequestVerifyRequestType = "VerifySolution";
exports.ILPConstants = ILPConstants;
class ILPRequestConstants {
}
ILPRequestConstants._assignmentId = "asgnmtId";
ILPRequestConstants._userSolution = "UserSolution";
ILPRequestConstants._courseName = "CourseShortName";
ILPRequestConstants._testCases = "TestCaseSolution";
ILPRequestConstants._assignmentType = "AsgnmtTyp";
ILPRequestConstants._solutionFileName = "SolutionFileName";
ILPRequestConstants._testCaseFileName = "TestcaseFileName";
ILPRequestConstants._hashTag = "HashTag";
exports.ILPRequestConstants = ILPRequestConstants;
class AnotherILPRequest {
    constructor() {
        this.handsOnType = "";
        this.moduleName = "";
        this.requestType = "";
        this.JSONInput = "";
    }
}
exports.AnotherILPRequest = AnotherILPRequest;
class ILPRequestNew {
    // public get HashTag() : string {
    //     return this._HashTag;
    // }
    // public set HashTag(v : string) {
    //     this._HashTag = v;
    // }
    constructor() {
        this.asgnmtId = "";
        // public get asgnmtId() : string {
        //     return this._asgnmtId;
        // }
        // public set asgnmtId(v : string) {
        //     this._asgnmtId = v;
        // }
        this.UserSolution = "";
        // public get UserSolution() : string {
        //     return this._UserSolution;
        // }
        // public set UserSolution(v : string) {
        //     this._UserSolution = v;
        // }
        this.CourseShortName = "";
        // public get CourseShortName() : string {
        //     return this._CourseShortName;
        // }
        // public set CourseShortName(v : string) {
        //     this._CourseShortName = v;
        // }
        // private _TestCaseSolution : string = "";
        // public get TestCaseSolution() : string {
        //     return this._TestCaseSolution;
        // }
        // public set TestCaseSolution(v : string) {
        //     this._TestCaseSolution = v;
        // }
        this.AsgnmtTyp = "";
        // public get AsgnmtTyp() : string {
        //     return this._AsgnmtTyp;
        // }
        // public set AsgnmtTyp(v : string) {
        //     this._AsgnmtTyp = v;
        // }
        this.SolutionFileName = "";
        // public get SolutionFileName() : string {
        //     return this._SolutionFileName;
        // }
        // public set SolutionFileName(v : string) {
        //     this._SolutionFileName = v;
        // }
        this.TestcaseFileName = "";
        // public get TestcaseFileName() : string {
        //     return this._TestcaseFileName;
        // }
        // public set TestcaseFileName(v : string) {
        //     this._TestcaseFileName = v;
        // }
        this.HashTag = "";
    }
}
exports.ILPRequestNew = ILPRequestNew;
class ILPRequest {
    constructor() {
        ILPRequest._assignmentId = "";
        ILPRequest._userSolution = "";
    }
    static get hashTag() {
        return ILPRequest._hashTag;
    }
    static set hashTag(v) {
        ILPRequest._hashTag = v;
    }
    static get testCaseFileName() {
        return ILPRequest._testCaseFileName;
    }
    static set testCaseFileName(v) {
        ILPRequest._testCaseFileName = v;
    }
    static get solutionFileName() {
        return ILPRequest._solutionFileName;
    }
    static set solutionFileName(v) {
        ILPRequest._solutionFileName = v;
    }
    static get assignmentType() {
        return ILPRequest._assignmentType;
    }
    static set assignmentType(v) {
        ILPRequest._assignmentType = v;
    }
    static get isFileSubmitted() {
        return ILPRequest._isFileSubmitted;
    }
    static set isFileSubmitted(v) {
        ILPRequest._isFileSubmitted = v;
    }
    static get isFileVerified() {
        return ILPRequest._isFileVerified;
    }
    static set isFileVerified(v) {
        ILPRequest._isFileVerified = v;
    }
    static get verifyType() {
        return ILPRequest._verifyType;
    }
    static set verifyType(v) {
        ILPRequest._verifyType = v;
    }
    static get fileLastModified() {
        return ILPRequest._fileLastModified;
    }
    static set fileLastModified(v) {
        ILPRequest._fileLastModified = v;
    }
    static get filePath() {
        return ILPRequest._filePath;
    }
    static set filePath(v) {
        ILPRequest._filePath = v;
    }
    static get courseName() {
        return ILPRequest._courseName;
    }
    static set courseName(v) {
        ILPRequest._courseName = v;
    }
    static get testCases() {
        return ILPRequest._testCases;
    }
    static set testCases(v) {
        ILPRequest._testCases = v;
    }
    static get userSolution() {
        return ILPRequest._userSolution;
    }
    static set userSolution(v) {
        ILPRequest._userSolution = v;
    }
    static get assignmentId() {
        return ILPRequest._assignmentId;
    }
    static set assignmentId(v) {
        ILPRequest._assignmentId = v;
    }
}
ILPRequest._isFileVerified = false;
ILPRequest._isFileSubmitted = false;
exports.ILPRequest = ILPRequest;
//# sourceMappingURL=ILPRequest.js.map