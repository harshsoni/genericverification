"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class VerifyRequest {
    constructor() {
        this.UserId = "";
        this.Code = "";
        this.QpName = "";
    }
}
exports.VerifyRequest = VerifyRequest;
class VerifyRequestStatic {
    constructor() {
        VerifyRequestStatic._userId = "";
        VerifyRequestStatic._userSolution = "";
        VerifyRequestStatic._hashTag = "";
    }
    static get hashTag() {
        return VerifyRequestStatic._hashTag;
    }
    static set hashTag(v) {
        VerifyRequestStatic._hashTag = v;
    }
    static get userId() {
        return VerifyRequestStatic._userId;
    }
    static set userId(v) {
        VerifyRequestStatic._userId = v;
    }
    static get userSolution() {
        return VerifyRequestStatic._userSolution;
    }
    static set userSolution(v) {
        VerifyRequestStatic._userSolution = v;
    }
}
exports.VerifyRequestStatic = VerifyRequestStatic;
class VerifyResponseSuggestionStatic {
    constructor() {
        VerifyResponseSuggestionStatic._testCaseName = "";
        VerifyResponseSuggestionStatic._lastNotVerified = "";
        VerifyResponseSuggestionStatic._lastNotVerifiedTimestamp = Date.now();
        VerifyResponseSuggestionStatic._firstVerified = "";
        VerifyResponseSuggestionStatic._firstVerifiedTimestamp = Date.now();
        VerifyResponseSuggestionStatic._methodSignature = "";
        VerifyResponseSuggestionStatic._suggestionId = 0;
    }
    static get methodSignature() {
        return VerifyResponseSuggestionStatic._methodSignature;
    }
    static set methodSignature(v) {
        VerifyResponseSuggestionStatic._methodSignature = v;
    }
    static get testCaseName() {
        return VerifyResponseSuggestionStatic._testCaseName;
    }
    static set testCaseName(v) {
        VerifyResponseSuggestionStatic._testCaseName = v;
    }
    static get lastNotVerified() {
        return VerifyResponseSuggestionStatic._lastNotVerified;
    }
    static set lastNotVerified(v) {
        VerifyResponseSuggestionStatic._lastNotVerified = v;
    }
    static get lastNotVerifiedTimestamp() {
        return VerifyResponseSuggestionStatic._lastNotVerifiedTimestamp;
    }
    static set lastNotVerifiedTimestamp(v) {
        VerifyResponseSuggestionStatic._lastNotVerifiedTimestamp = v;
    }
    static get firstVerified() {
        return VerifyResponseSuggestionStatic._firstVerified;
    }
    static set firstVerified(v) {
        VerifyResponseSuggestionStatic._firstVerified = v;
    }
    static get firstVerifiedTimestamp() {
        return VerifyResponseSuggestionStatic._firstVerifiedTimestamp;
    }
    static set firstVerifiedTimestamp(v) {
        VerifyResponseSuggestionStatic._firstVerifiedTimestamp = v;
    }
    static get suggestionId() {
        return VerifyResponseSuggestionStatic._suggestionId;
    }
    static set suggestionId(v) {
        VerifyResponseSuggestionStatic._suggestionId = v;
    }
}
exports.VerifyResponseSuggestionStatic = VerifyResponseSuggestionStatic;
class VerifyResponseStructuralTestCase {
    constructor() {
        VerifyResponseStructuralTestCase._methodSignature = "";
        VerifyResponseStructuralTestCase._isPassed = false;
    }
    static get methodSignature() {
        return VerifyResponseStructuralTestCase._methodSignature;
    }
    static set methodSignature(v) {
        VerifyResponseStructuralTestCase._methodSignature = v;
    }
    static get isPassed() {
        return VerifyResponseStructuralTestCase._isPassed;
    }
    static set isPassed(v) {
        VerifyResponseStructuralTestCase._isPassed = v;
    }
}
exports.VerifyResponseStructuralTestCase = VerifyResponseStructuralTestCase;
class VerifyResponseLogicalTestCase {
    constructor() {
        VerifyResponseLogicalTestCase._methodSignature = "";
        VerifyResponseLogicalTestCase._testCaseName = "";
        VerifyResponseLogicalTestCase._isPassed = false;
        VerifyResponseLogicalTestCase._actual = "";
        VerifyResponseLogicalTestCase._expected = "";
    }
    static get testCaseName() {
        return VerifyResponseLogicalTestCase._testCaseName;
    }
    static set testCaseName(v) {
        VerifyResponseLogicalTestCase._testCaseName = v;
    }
    static get methodSignature() {
        return VerifyResponseLogicalTestCase._methodSignature;
    }
    static set methodSignature(v) {
        VerifyResponseLogicalTestCase._methodSignature = v;
    }
    static get isPassed() {
        return VerifyResponseLogicalTestCase._isPassed;
    }
    static set isPassed(v) {
        VerifyResponseLogicalTestCase._isPassed = v;
    }
    static get actual() {
        return VerifyResponseLogicalTestCase._actual;
    }
    static set actual(v) {
        VerifyResponseLogicalTestCase._actual = v;
    }
    static get expected() {
        return VerifyResponseLogicalTestCase._expected;
    }
    static set expected(v) {
        VerifyResponseLogicalTestCase._expected = v;
    }
}
exports.VerifyResponseLogicalTestCase = VerifyResponseLogicalTestCase;
class VerifyResponseRootStatic {
    static get hashTag() {
        return VerifyResponseRootStatic._hashTag;
    }
    static set hashTag(v) {
        VerifyResponseRootStatic._hashTag = v;
    }
    static get logical() {
        return VerifyResponseRootStatic._logical;
    }
    static set logical(v) {
        VerifyResponseRootStatic._logical = v;
    }
    static get structural() {
        return VerifyResponseRootStatic._structural;
    }
    static set structural(v) {
        VerifyResponseRootStatic._structural = v;
    }
    static get suggestions() {
        return VerifyResponseRootStatic._suggestions;
    }
    static set suggestions(v) {
        VerifyResponseRootStatic._suggestions = v;
    }
}
exports.VerifyResponseRootStatic = VerifyResponseRootStatic;
//# sourceMappingURL=VerifyRequest.js.map