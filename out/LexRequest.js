"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class LexRequest {
    constructor() {
        this.user_solution = "";
        this.user_id_type = "";
    }
}
exports.LexRequest = LexRequest;
class LexResponse {
    constructor() {
        this.verifyResult = "";
    }
}
exports.LexResponse = LexResponse;
class LexRequestConstants {
}
LexRequestConstants._user_id_type = "email";
//public static _req_url:string = "10.177.157.28:7001/v1/users/pratik.shetty@ad.infosys.com/exercises/lex_auth_0127043999804047361035/python-submission?type=verify";
LexRequestConstants._client_id = "1012";
LexRequestConstants._api_key = "7053b6d56fdb18edbf04fe3076647a67";
LexRequestConstants._req_host = "10.177.157.28";
LexRequestConstants._req_port = "7001";
LexRequestConstants._req_exam_host = "10.177.157.28";
LexRequestConstants._req_exam_port = "7001";
LexRequestConstants._req_dummy_path = "/v1/users/pratik.shetty@ad.infosys.com/exercises/lex_auth_0127043999804047361035/python-submission/?type=verify";
LexRequestConstants._req_path_substr1 = "/v1/users/";
LexRequestConstants._req_path_substr2 = "/exercises/";
LexRequestConstants._req_path_substr3 = "/python-submission/?type=verify";
exports.LexRequestConstants = LexRequestConstants;
//# sourceMappingURL=LexRequest.js.map